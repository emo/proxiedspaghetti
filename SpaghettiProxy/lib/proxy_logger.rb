#!ruby -Ku

# 同期設定
USE_LOG_MUTEX = true

require 'mutex_m' if USE_LOG_MUTEX

# Loggerの場合は色々と不便なため自前で作成.
class ProxyLogger
  include Mutex_m if USE_LOG_MUTEX
  
  # log-level
  DEBUG = 0
  INFO  = 1
  WARN  = 2
  ERROR = 3
  FATAL = 4
  
  # file finalyzer
  def self.close_file files
    proc{
      files.each{|f| f.close unless f.closed? }
    }
  end
  
  # outに指定した先に出力する
    # IOでない場合はファイルとして開く
  def initialize out = [], loglevel = INFO
    super() # Mutex_mの初期化
    
    @level = loglevel
    
    files = []
    @out = out.collect{ |o|
      files << (o = open(o, "a")) unless o.is_a?(IO)
      o
    }
    
    @def_progname = "server"
    w2 "date,progname,severity,message"
    
    ObjectSpace.define_finalizer(self, self.class.close_file(files))
  end
  attr_accessor :def_progname, :level
  
  if USE_LOG_MUTEX
    def log_mutex; mu_synchronize{ yield }; end
  else
    def log_mutex; yield; end
  end
  
  # write out
  def w msg
    log_mutex{ @out.each{ |o| o.puts msg } }
  end
  
  # debug write out. syncでの待ち時間を表示する.
  # nextlineは2行以上のログのため.
  def w2 msg, nextline = ""
    t = Time.now
    log_mutex{
      msg = "%s,%.3fs late%s" % [msg, (Time.now - t), nextline]
      @out.each{ |o| o.puts msg }
    }
  end
  
  
  def log severity, progname = @def_progname, &log_mes_gen
    datetime = Time.now
    
    mes, nextline = if (logdata = log_mes_gen.call).is_a?(Enumerable) then
      a = logdata.to_a
      n = a[1..-1].collect {|s| "-,-,-,#{s},-" }.join("\n")
      [a[0], "\n" + n]
    else
      [logdata, ""]
    end
    
    w2("%s.%03d,%s,%s,%s" % [
      (datetime.strftime '%H:%M:%S'), (datetime.usec / 1000),
      progname, severity, mes
    ], nextline)
  end
  
  # 記録用. メタプロするとIDEが補間してくれない．
  def fatal progname = @def_progname, &log_mes_gen
    log("FATAL", progname, &log_mes_gen) if @level <= FATAL
  end
  def error progname = @def_progname, &log_mes_gen
    log("ERROR", progname, &log_mes_gen) if @level <= ERROR
  end
  def warn progname = @def_progname, &log_mes_gen
    log("WARN", progname, &log_mes_gen) if @level <= WARN
  end
  def info progname = @def_progname, &log_mes_gen
    log("INFO", progname, &log_mes_gen) if @level <= INFO
  end
  def debug progname = @def_progname, &log_mes_gen
    log("DEBUG", progname, &log_mes_gen) if @level <= DEBUG
  end
  
  def error_exception place, exception, progname = @def_progname
    error(progname){ "#{place} : #{exception.inspect} in #{$@.inspect}" }
  end
  
  # Webrickのログ出力用
  def << obj
    info("webrick"){ obj.to_s.chomp }
  end
  
end








