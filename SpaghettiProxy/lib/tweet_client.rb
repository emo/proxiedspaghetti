#!ruby -Ku
# 
# Twitter関連の処理をする部分のライブラリ

require "twitter"
require "yaml"
require 'twitter' # gem

# SSL接続がうまくいかない場合のみ
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

# Twitter Clientを返す
def init_twitter token_file = "token.yml"
  token = YAML.load_file(token_file)
  Twitter.configure { |conf|
    [:consumer_key , :consumer_secret].each{ |k|
      conf.__send__("#{k}=", token[k])
    }
  }
  Twitter::Client.new token
end


# postしてみるテスト
if $0 == __FILE__
  tc = init_twitter
  p tc.update("tweeting test at #{Time.now.to_i}")[:attrs]
end



