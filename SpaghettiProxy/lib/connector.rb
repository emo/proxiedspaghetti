#!ruby -Ku
# 
# 接続用設定ファイルの読み書きと,送受信内容の暗号化/復号化を行う

require 'yaml'
require 'net/http'
require 'openssl'
require 'digest/sha2'
require 'base64'

# 送信用にbase64エンコード. 改行の削除も.
def b64enc data
	Base64.encode64(data).gsub(/\n/, '')
end

class Connector
  # 初期化時にどちらにせよ必要になる暗号化/復号化部分を生成(keyは後付)
  def initialize
    @enc, @dec = [:encrypt, :decrypt].collect {|mt|
      r = OpenSSL::Cipher.new "AES-256-CBC"
      r.padding = 1
      r.__send__ mt # ここでencryptがdecryptか決定
      r
    }
  end
  
  # 読み込むか生成するかしたKeyとIVを暗号/復号機につっこむ
  def load_key_and_iv
    [@enc, @dec].each{ |cpr| cpr.key = @key; cpr.iv = @iv }
  end
  private :load_key_and_iv
  
  
  # 設定をファイルから読み込み
  def load file
    conf = YAML.load_file file
    @url = URI conf[:url]
    @key, @iv = conf[:key], conf[:iv]
    load_key_and_iv
  end
  attr_reader :url
  
  # WAN側IP取得
  # => Post URL
  def self.get_wan_ip
    u = URI 'http://checkip.dyndns.org/'
    return (Net::HTTP.get(u) =~ /(\d+\.\d+\.\d+\.\d+)/) && $1
  end
  
  # IPアドレスとkey+ivを取得(生成)して保存
  def save file, port, local = false
    if local
      @ip = '127.0.0.1'
    else
      raise "failed to get WAN-IP" unless @ip = self.class.get_wan_ip
    end
    
    rd = Random.new
    conf = {
      :url => "http://#{@ip}:#{port}/",
      :key => b64enc(@key = rd.bytes(@enc.key_len)),
      :iv => b64enc(@iv = rd.bytes(@enc.iv_len)),
    }
    #p @key, @iv
    load_key_and_iv
    IO.write file, YAML.dump(conf)
    conf[:url]
  end
  
  
  # 使用HASHの設定.
  HASHALG = Digest::SHA256
  HASHSIZE = 256 / 8
  
  # 暗号化してHASHも添加する. 元データはエンコードが破壊的に変更される.
  def encrypt data
    data = data.force_encoding Encoding::ASCII_8BIT
    @enc.iv = @iv # IV初期化は毎回行う
    return @enc.update(data + HASHALG.digest(data)) + @enc.final
  end
  
  # 復号化し更にHASHの確認を行い,正しければ平文,間違っていればfalseを返す.
  def decrypt data
    @dec.iv = @iv # IV初期化は毎回行う
    plain = @dec.update(data) + @dec.final
    len = plain.size - HASHSIZE
    body, csum = plain[0...len], plain[len..-1]
    return (csum == HASHALG.digest(body)) && body
  end
  
  # 指定したPathの要求を送信
  def get path
    decrypt Net::HTTP.get(@url + path)
  end
end



