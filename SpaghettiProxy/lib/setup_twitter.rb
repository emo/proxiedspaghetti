#!ruby -Ku

# consumer keyとtokenを取得して保存するだけのツール
# 参考 : http://qiita.com/items/b147a8fb20b8ff3286b5
# 元 : https://github.com/jugyo/get-twitter-oauth-token/blob/master/bin/get-twitter-oauth-token

require "twitter"
require "yaml"
require "oauth"


r = {}

print "Consumer key >"
r[:consumer_key] = gets.strip

print "Consumer secret >"
r[:consumer_secret] = gets.strip

consumer = OAuth::Consumer.new(
  r[:consumer_key], r[:consumer_secret],
  :site => "http://api.twitter.com/"
)

request = consumer.get_request_token
a_url = request.authorize_url

system('open', a_url) || puts("Access here:\n#{a_url}\n")

print "PIN >"
pin = gets.strip

token = request.get_access_token(
  :oauth_token => request.token,
  :oauth_verifier => pin
)

r[:oauth_token] = token.token
r[:oauth_token_secret] = token.secret

IO.write("token.yml", r.to_yaml)

puts "saved"





