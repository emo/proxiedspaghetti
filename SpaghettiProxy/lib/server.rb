#!ruby -Ku

# default library
require "yaml"
require "webrick"
require 'pathname'
require 'kconv'

# set current directory
ProgDir = Pathname(__FILE__).dirname

# my lib
require ProgDir + 'tweet_client'
require ProgDir + 'proxy_logger'
require ProgDir + 'connector'

class TweetProxyServer
  # local modeで動作させるかどうか
  def initialize config_file
    @conf = YAML.load_file config_file
    
    # logger設定
    loglv = ProxyLogger.const_get @conf[:loglevel]
    (logd = Pathname(@conf[:logdir])).mkpath
    logf = logd + "#{Time.now.to_i}.log.csv"
    @log = ProxyLogger.new([logf, STDOUT], loglv)
    
    # Twitter setup
    @twc = init_twitter(ProgDir + "token.yml")
    
    # connector setup
    @cnt = Connector.new
    post_url = @cnt.save(* @conf.values_at(:saveip, :port, :debug_local) )
    @log.info{ "post URL : #{post_url}" }
    
    # HTTP Server setup
    @srv = WEBrick::HTTPServer.new(
      :Port => @conf[:port], :AccessLog => [ [@log, '%h "%r" %s %b'] ]
    )
    @srv.mount_proc("/") { |req, res| accept_request(req,res) }
  end
  
  def start
    @log.info{ "server start" }
    trap("INT"){
      @log.info{ "trap interrupt and shutdown" }
      @srv.shutdown
    }
    @srv.start
  end
  
  # 受信時処理
  def accept_request req, res
    @log.info{
      [ "Received: #{req.object_id}",
        "Method:#{req.request_method}",
        "Path:#{req.path} ",
        "body:#{(req.body||"").size} bytes"
      ].join(" ")
    }
    
    case req.request_method
    when "POST"
      accept_post(req, res)
    when "GET"
      res.body = "post here"
    end
    
    @log.info{ "return: #{req.object_id}" }
  end
  
  # post結果を返す
  def set_post_result res, status, detail
    data = {"status" => status, "detail" => detail}.to_yaml
    res.body = @cnt.encrypt(data)
    @log.debug{ "result: #{data.size} bytes (enc: #{res.body.size} bytes)" }
    #@log.debug{ "assert: #{data == @kc.decrypt(res.body)}" }
    #@log.debug{ "digest: #{Digest::SHA256.hexdigest(res.body)}"}
  end
  
  # Post
  def accept_post req, res
    plain = @cnt.decrypt(req.body).toutf8
    @log.info{ "post data decrypt : " + (plain ? plain.inspect : "Error") }
    
    stt = @twc.update(plain)[:attrs]
    @log.debug{ "tweeted: #{stt[:id_str]}" }
    set_post_result(res, "ok", {}) # IDがint32でパースできないので何も送らない
  rescue => e
    @log.error{ ["post error : #{e}", *$@] }
    set_post_result(res, "error", b64enc(e.to_s))
  end
  
end


srv = TweetProxyServer.new(ProgDir + "config.yml")
srv.start

