#!ruby -Ku
$:.unshift File.join(File.dirname(__FILE__),'..','lib')

require 'test/unit'
require 'keychecker'
require 'digest/md5'
require "kconv"

class KeyChecker_Tester < Test::Unit::TestCase
  def setup
    @kc = KeyChecker.new
    @kc.set_key_and_iv
    @rd = Random.new
  end
  
  # Encrypt → Decryptで戻るかの試験
  def enc_and_dec plain
    ciphered = @kc.encrypt(plain)
    plain2 = @kc.decrypt(ciphered)
    assert_equal(plain, plain2)
  end
  
  # マルチバイト用
  def enc_and_dec_utf8 plain
    plain = plain.toutf8 # test用に統一
    ciphered = @kc.encrypt_mb(plain)
    plain2 = @kc.decrypt_mb(ciphered)
    assert_equal(plain, plain2)
  end
  
  
  def test_decrypt
    # サイズを変更しながらbinaryで試験
    (0..16).each{|s|
      enc_and_dec @rd.bytes(131+s)
      #@kc.set_key_and_iv
    }
    enc_and_dec "ABC"*37 # stringで試験
    enc_and_dec_utf8("もじれつ") # multi-byteで試験
  end
  
end
