﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Yaml;
using System.IO;

namespace ProxiedSpaghetti
{
	// YAMLの読み込みとか
	public static class YamlUtil
	{
		public static string getString(this YamlNode yn) {
			return ((YamlScalar)yn).Value;
		}
		public static int getInt(this YamlNode yn) {
			return int.Parse(yn.getString());
		}
		public static byte[] getByteFromBase64(this YamlNode yn) {
			return Convert.FromBase64String(yn.getString());
		}
		// UTF8をBase64encodeしたものからstringを取得
		public static string getStringFromBase64(this YamlNode yn) {
			return Encoding.UTF8.GetString(yn.getByteFromBase64());
		}
		/* // いらんかった
		public static byte[] getBytes(this YamlNode yn)
		{
			return Encoding.ASCII.GetBytes(yn.getString());
		}
		 */
		public static YamlMapping topMap(this YamlNode[] yn)
		{
			return (YamlMapping)yn[0];
		}
		
	}

	// 設定. 基本的にstatic
	public class Config
	{
		private static string serverConfFile;

		// これはAccessごとに読み込みInstance生成
		public class ServerConfig
		{
			public string	post_url { set; get; }
			public byte[]	key { set; get; }
			public byte[]	iv { set; get; }
		}
		// こっちは起動時読み込みのみ
		public class ClientConfig
		{
			public int timeout { set; get; }
			public int retry { set; get; }
		}
		public static ClientConfig cconf;

		public static void loadConfig(string path)
		{
			// 全体設定読み込み
			var yn = YamlNode.FromYamlFile(path).topMap();
			serverConfFile = yn["server_setting"].getString();

			// client設定読み込み
			cconf = new ClientConfig();
			var clc = (YamlMapping)yn["client"];
			cconf.timeout = clc["timeout"].getInt();
			cconf.retry = clc["retry"].getInt();
		}

		// Post先設定読み込み. 基本的にPostごとに読み込み.
		public static ServerConfig loadServerConfig()
		{
			var r = new ServerConfig();
			var yn = YamlNode.FromYamlFile(serverConfFile).topMap();
			r.post_url = yn[":url"].getString();
			r.key = yn[":key"].getByteFromBase64();
			r.iv = yn[":iv"].getByteFromBase64();
			return r;
		}
	}

}
