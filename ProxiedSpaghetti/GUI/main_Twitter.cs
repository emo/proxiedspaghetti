﻿using System;
using System.Diagnostics;
using System.Threading;
//using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Yaml;

namespace ProxiedSpaghetti
{
	using statusViewArg = Tuple<string, main.statusType>; // status表示用

	public partial class main
	{
		// Post後にホットキー入力があるため,一度だけ無効化する.
		private void postText_ChangeInvalid(object sender, EventArgs e)
		{
			postText.TextChanged -= postText_ChangeInvalid;
			postText.TextChanged += textBox1_TextChanged;
			postText.Text = clearTextValue; // こっちの変更で元の textBox1_TextChanged が来る.
		}

		Poster poster;
		string clearTextValue;

		// Post部分初期化
		private void initPoster() { poster = new Poster(); }

		// Postする
		private void postSequence()
		{
			if (postText.Text == "") return; // 空なら無視

			// post開始表示
			viewStatus("tweeting...", statusType.progress);
			postText.Enabled = false;

			// postして結果表示
			var v = postResultView();
			viewStatus(v.Item1, v.Item2);

			// postに成功した場合はクリア, そうでなければテキストをそのままにする
			// ただし改行は削除する必要があるため,postするはずだったテキストで上書きする方法をとる
			clearTextValue = (v.Item2 == statusType.status) ? "" : postText.Text;

			// 無効化解除
			postText.Enabled = true;
			postText.Focus();
			this.Visible = false;
			
			// ホットキーの入力がこの後で処理されるため,それを無効化しテキストもクリアする.
			postText.TextChanged -= textBox1_TextChanged;
			postText.TextChanged += postText_ChangeInvalid;
		}

		// errorを返却. WriteLineもする.
		private statusViewArg errorStatus(string mes)
		{
			Debug.WriteLine(mes);
			return new statusViewArg(mes, statusType.error);
		}


		// postして表示するメッセージを返す
		private statusViewArg postResultView()
		{
			Util.DebugU.writeDateAndBar(" post start ");
			// postする
			string res = null;
			try
			{
				res = poster.post(postText.Text);
			}
			catch (System.Net.WebException we)
			{
				return errorStatus("web exception: " + we.Status);
			}
			catch (System.Security.Cryptography.CryptographicException ce)
			{
				return errorStatus("crypt failed: " + ce.Message);
			}
			catch (Poster.serverUnaccessable)
			{
				return errorStatus("server unaccessable");
			}
			
			// resがnull(=復号化失敗時?)
			if (res == null) return errorStatus("error: decrypt error ?");

			// 相手からメッセージが帰ってきた場合のエラーメッセージ
			var ym = YamlNode.FromYaml(res).topMap();
			var stt = ym["status"].getString();
			Debug.WriteLine("tweet: " + stt);
			switch (stt)
			{
				case "error":
					var mes = ym["detail"].getStringFromBase64();
					if (mes == "Status is a duplicate.") mes = "duplicate";
					return errorStatus("error: " + mes);
				case "ok":
					return new statusViewArg("tweet successfully", statusType.status);
			}
			return new statusViewArg("Unknown", statusType.warning);
		}
		

	}
}
