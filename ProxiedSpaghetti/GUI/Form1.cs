﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using ProxiedSpaghetti.Properties;

namespace ProxiedSpaghetti
{
	public partial class main : Form
	{
		// マウスドラッグで移動できるようにする
		partial void mousedown(object sender, MouseEventArgs e);
		partial void mousemove(object sender, MouseEventArgs e);

		// ホットキーでのイベント. 浮上 <=> 沈没.
		private void floatingWindow(object sender, EventArgs e)
		{
			this.Visible ^= true;
			// 可視化した場合はアクティブに
			if (this.Visible) {
				this.Activate();
				this.postText.Focus();
			}
		}

		public main()
		{
			InitializeComponent();

			// twitter post部分初期化
			initPoster();
			// UI初期表示
			updateTweetLength();
		}


		
		private void main_Load(object sender, EventArgs e)
		{
			setHotkey();

			this.Bounds = Settings.Default.Bounds; // サイズ・位置の復元
		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{
			updateTweetLength();
			//MyDebugger.WLT("TextChanged :" + this.postText.Text.Replace("\r\n","\\n"));
		}

		private void statusLabel_Click(object sender, EventArgs e)
		{

		}

		private void textBox1_KeyDown(object sender, KeyEventArgs e)
		{
			// ショートカットキーの設定
			if (e.Control)
			{
				//MyDebugger.WLT("Ctrl KeyEvent Start, " +e.ToDebugString());
				switch (e.KeyCode)
				{
					case Keys.A:
						postText.SelectAll();
						break;
					case Keys.Enter:
						postSequence();
						break;
				}
				//MyDebugger.WLT("Ctrl KeyEvent End, " + e.ToDebugString());
			}
			
		}

		// Resize用枠線切り替え
		partial void changeFormBorder(object sender, EventArgs e);

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void main_FormClosing(object sender, FormClosingEventArgs e)
		{
			Settings.Default.Bounds = this.Bounds; // サイズ・位置保存
			Settings.Default.Save();
		}

		private void ViewTrauMenu(object sender, EventArgs e)
		{
			// 左クリックでメニュー出したいけど出てくる場所がずれる
			//this.trayMenu.Show();
		}

	}
}
