﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace ProxiedSpaghetti
{
	static class Program
	{
		static public void UnhandledExceptionCatcher
			(object sender, UnhandledExceptionEventArgs e)
		{
			Debug.WriteLine(e.ToString(), "UnhandledExceptionEventArgs");
		}
	
		/// <summary>
		/// アプリケーションのメイン エントリ ポイントです。
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			Config.loadConfig("config.yml");

			var m = new main();
			Application.Run(m);
		}
	}
}
