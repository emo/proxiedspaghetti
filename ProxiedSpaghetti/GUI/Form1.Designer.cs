﻿namespace ProxiedSpaghetti
{
	partial class main
	{
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		/// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows フォーム デザイナーで生成されたコード

		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(main));
			this.postText = new System.Windows.Forms.TextBox();
			this.lengthLabel = new System.Windows.Forms.Label();
			this.trayMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.statusLabel = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.trayIcon = new System.Windows.Forms.NotifyIcon(this.components);
			this.trayMenu.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// postText
			// 
			this.postText.BackColor = System.Drawing.Color.Black;
			this.postText.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.postText.Dock = System.Windows.Forms.DockStyle.Fill;
			this.postText.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.postText.ForeColor = System.Drawing.Color.PaleTurquoise;
			this.postText.Location = new System.Drawing.Point(0, 0);
			this.postText.Multiline = true;
			this.postText.Name = "postText";
			this.postText.Size = new System.Drawing.Size(220, 88);
			this.postText.TabIndex = 0;
			this.postText.TabStop = false;
			this.postText.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			this.postText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
			this.postText.MouseDown += new System.Windows.Forms.MouseEventHandler(this.mousedown);
			this.postText.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mousemove);
			// 
			// lengthLabel
			// 
			this.lengthLabel.AutoSize = true;
			this.lengthLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.lengthLabel.ContextMenuStrip = this.trayMenu;
			this.lengthLabel.Cursor = System.Windows.Forms.Cursors.Default;
			this.lengthLabel.Dock = System.Windows.Forms.DockStyle.Right;
			this.lengthLabel.ForeColor = System.Drawing.Color.Lime;
			this.lengthLabel.Location = new System.Drawing.Point(197, 0);
			this.lengthLabel.Name = "lengthLabel";
			this.lengthLabel.Size = new System.Drawing.Size(23, 12);
			this.lengthLabel.TabIndex = 1;
			this.lengthLabel.Text = "---";
			this.lengthLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.lengthLabel.Click += new System.EventHandler(this.changeFormBorder);
			// 
			// trayMenu
			// 
			this.trayMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
			this.trayMenu.Name = "trayMenu";
			this.trayMenu.Size = new System.Drawing.Size(121, 26);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
			this.exitToolStripMenuItem.Text = "exit (&X)";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// statusLabel
			// 
			this.statusLabel.AutoSize = true;
			this.statusLabel.BackColor = System.Drawing.Color.Black;
			this.statusLabel.Cursor = System.Windows.Forms.Cursors.Default;
			this.statusLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.statusLabel.ForeColor = System.Drawing.Color.Lime;
			this.statusLabel.Location = new System.Drawing.Point(0, 0);
			this.statusLabel.Name = "statusLabel";
			this.statusLabel.Size = new System.Drawing.Size(25, 12);
			this.statusLabel.TabIndex = 1;
			this.statusLabel.Text = "     ";
			this.statusLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.statusLabel.Click += new System.EventHandler(this.statusLabel_Click);
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.AutoSize = true;
			this.panel1.BackColor = System.Drawing.Color.Black;
			this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.panel1.Controls.Add(this.statusLabel);
			this.panel1.Controls.Add(this.lengthLabel);
			this.panel1.Location = new System.Drawing.Point(0, 76);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(220, 12);
			this.panel1.TabIndex = 2;
			// 
			// trayIcon
			// 
			this.trayIcon.ContextMenuStrip = this.trayMenu;
			this.trayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("trayIcon.Icon")));
			this.trayIcon.Text = "ゲティ海洋P";
			this.trayIcon.Visible = true;
			this.trayIcon.Click += new System.EventHandler(this.ViewTrauMenu);
			// 
			// main
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.DimGray;
			this.ClientSize = new System.Drawing.Size(220, 88);
			this.ControlBox = false;
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.postText);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = System.Windows.Forms.ImeMode.On;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "main";
			this.Opacity = 0.7D;
			this.ShowInTaskbar = false;
			this.TopMost = true;
			this.TransparencyKey = System.Drawing.Color.Transparent;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.main_FormClosing);
			this.Load += new System.EventHandler(this.main_Load);
			this.trayMenu.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox postText;
		private System.Windows.Forms.Label lengthLabel;
		private System.Windows.Forms.Label statusLabel;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.NotifyIcon trayIcon;
		private System.Windows.Forms.ContextMenuStrip trayMenu;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
	}
}

