﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;

using ProxiedSpaghetti.Properties;


// UIを動かす時のMethodとか
namespace ProxiedSpaghetti
{
	// Twitterにおいての文字数制限の判断
	static class TwitterUtil
	{
		// URLにマッチする正規表現
		static readonly Regex regURL = new Regex(Resources.URLRegex);

		// URLを22(httpsは23)文字として扱い、文字数を算出する
		public static int TweetLength(this string s)
		{
			// プロトコルを見れてるか確認する
			//foreach (Match m in regURL.Matches(s)) Debug.WriteLine(m.Groups[1]);
			return s.Length +
				regURL.Matches(s).Cast<Match>().
				Sum(mt => mt.Groups[1].Value.Length + 18 - mt.Length);
		}
	}

	// Mainの部分
	public partial class main
	{
		// status表示の種類
		public enum statusType
		{
			status,
			progress,
			warning,
			error
		}

		// status表示.種類で色を変更.
		private void viewStatus(string text, statusType type)
		{
			statusLabel.ForeColor =
				(type == statusType.status) ? Color.Lime :
				(type == statusType.progress) ? Color.DodgerBlue :
				(type == statusType.warning) ? Color.Yellow :
				(type == statusType.error) ? Color.Red :
				Color.White;
			statusLabel.Text = text;
			statusLabel.Update();
		}

		// マウスドラッグで移動できるようにする
		private Point mdp; // mouse down point
		partial void mousedown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
				mdp = new Point(e.X, e.Y);
		}
		partial void mousemove(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
				Location += new Size(e.X - mdp.X, e.Y - mdp.Y);
		}

		// Resize用枠線切り替え
		partial void changeFormBorder(object sender, EventArgs e)
		{
			this.FormBorderStyle =
				(this.FormBorderStyle == FormBorderStyle.None)
				? FormBorderStyle.Sizable
				: FormBorderStyle.None;
		}

		// twitterの代表的な定数
		public const int maxTweetLength = 140;

		// テキスト変更時
		private void updateTweetLength()
		{
			int twl = postText.Text.TweetLength();
			lengthLabel.ForeColor =
				(twl == 0) ? Color.Gray :
				(twl <= maxTweetLength) ? Color.Lime : Color.Red;
			lengthLabel.Text = twl + "/" + maxTweetLength;
			//statusLabel.Text = "";
		}

		// ホットキー登録用
		GlobalHotkey ghk;
		
		// ホットキー登録
		private void setHotkey()
		{
			try {
				ghk = new GlobalHotkey(GlobalHotkey.ModifierKeys.Alt, Keys.S);
				ghk.keyEvent += floatingWindow;
			}
			catch (GlobalHotkey.HotkeyRegistrationException) {
				viewStatus("Hotkey registration failed.", statusType.warning);
			}
		}

	}
}
