﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Yaml;
using System.Diagnostics;

namespace Util
{
	// 配列操作とか
	static class ArrayUtil
	{
		// 部分配列生成
		public static byte[] subArray(this byte[] baseArray, int index, int length)
		{
			var r = new byte[length];
			Array.Copy(baseArray, index, r, 0, length);
			return r;
		}
		// HEX化
		public static string toHEX(this byte[] binary)
		{
			var r = new StringBuilder();
			r.Capacity = binary.Length * 2;
			foreach (byte b in binary)
				r.Append(String.Format("{0:X2}", b));
			return r.ToString();
		}

	}

	/// <summary>
	/// ファイル操作色々.
	/// 主に簡略化のため.
	/// </summary>
	static class FileIO
	{

	}

	// debug用
	static class DebugU
	{
		public static void writeDateAndBar(string message = "")
		{
			Debug.WriteLine(
				DateTime.Now.ToString("HH:mm:ss") +
				" --------" + message + "--------"
			);
		}
	}
}
