﻿using System.Security.Cryptography;
using System.Text;
using System.Linq;
using Util;
using System.Diagnostics;

namespace Encrypt
{
	// AESの自分用設定
	class AES
	{
		static readonly Encoding chEnc = Encoding.UTF8;

		private AesCryptoServiceProvider	aes;
		private byte[] myiv; // IVは毎回初期化するため
		//static readonly SHA256 hashmaker = new SHA256Managed(); // Hash生成用
		SHA256 hashmaker;
		const int hashSize = 256 / 8;
		
		public AES()
		{
			aes = new AesCryptoServiceProvider ();
			aes.BlockSize = 128;
			aes.KeySize = 256;
			aes.Mode = CipherMode.CBC;
			aes.Padding = PaddingMode.PKCS7;

			hashmaker = new SHA256Managed();
		}
		public void set_key(byte[] key, byte[] iv)
		{
			aes.IV = myiv = iv;
			aes.Key = key;
		}


		public byte[] encrypt(byte[] plainText)
		{
			aes.IV = myiv;
			using (var enc = aes.CreateEncryptor())
				return enc.TransformFinalBlock(plainText, 0, plainText.Length);
		}
		public byte[] encrypt(string plainText)
		{
			return encrypt(chEnc.GetBytes(plainText));
		}
		public byte[] decrypt(byte[] cipherText)
		{
			aes.IV = myiv;
			using (var dec = aes.CreateDecryptor())
				return dec.TransformFinalBlock(cipherText, 0, cipherText.Length);
		}
		public string decryptToString(byte[] cipherText)
		{
			return chEnc.GetString(decrypt(cipherText));
		}


		// SHA256 Hashを添付して暗号化
		public byte[] hashAttachedEncrypt(byte[] plainText)
		{
			var hash = hashmaker.ComputeHash(plainText);
			var pt = plainText.Concat(hash).ToArray();
			return encrypt( pt );
		}
		public byte[] hashAttachedEncrypt(string plainText)
		{
			return hashAttachedEncrypt(chEnc.GetBytes(plainText));
		}

		// SHA256 Hashを添付して暗号化されたものを復号化. 一致しないならnull.
		public byte[] hashAttachedDecrypt(byte[] cipherText)
		{
			if(cipherText.Length < hashSize) return null;
			//Debug.WriteLine(hashmaker.ComputeHash(cipherText).toHEX()); // post内容の確認
			var plain1 = decrypt(cipherText); // まず復号化
			// 復号化内容から切り出し
			int sep = plain1.Length - hashSize;
			var plain2 = plain1.subArray(0, sep);
			var hash1 = plain1.subArray(sep, hashSize);
			var hash2 = hashmaker.ComputeHash(plain2);
			return hash1.SequenceEqual(hash2) ? plain2 : null;
		}
		public string hashAttachedDecryptToString(byte[] cipherText)
		{
			var r = hashAttachedDecrypt(cipherText);
			return (r == null) ? null : chEnc.GetString(r);
		}
	}

}
