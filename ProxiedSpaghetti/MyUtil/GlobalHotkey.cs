﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using System.Runtime.InteropServices;
//using System.Windows.Input;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;

/// <summary>
/// Global Hotkeyを設定するclass
/// 1 instance / key
/// </summary>
class GlobalHotkey : IDisposable
{
	// 呼ばれるイベント
	public event EventHandler keyEvent;


	// 修飾キー
	[FlagsAttribute]
	public enum ModifierKeys : int
	{
		Alt = 1, Ctrl = 2, Shift = 4,
	}

	// 登録と呼び出しをするForm
	KeyForm kh;

	// 登録
	public GlobalHotkey(ModifierKeys modKeys, Keys keys)
	{
		kh = new KeyForm(modKeys, keys, () => keyEvent(this, EventArgs.Empty));
	}

	// 登録解除
	public void Dispose() { this.kh.Dispose(); }


	// 登録失敗時の例外
	public class HotkeyRegistrationException : Exception
	{
		public HotkeyRegistrationException() {}
		public HotkeyRegistrationException(string message) : base(message) {}
		public HotkeyRegistrationException(string message, System.Exception inner) :
			base(message, inner) {}
		protected HotkeyRegistrationException(
			System.Runtime.Serialization.SerializationInfo info,
			System.Runtime.Serialization.StreamingContext context
		) { }

	}

	/// <summary>
	/// ホットキーイベントを登録し呼び出すForm
	/// </summary>
	private class KeyForm : Form
	{
		[DllImport("user32.dll")]
		extern static int RegisterHotKey(IntPtr HWnd, int ID, ModifierKeys modKeys, Keys keys);

		[DllImport("user32.dll")]
		extern static int UnregisterHotKey(IntPtr HWnd, int ID);

		const int WM_HOTKEY = 0x0312;
		
		int id;
		ThreadStart proc;

		public KeyForm(ModifierKeys modKeys, Keys keys, ThreadStart proc)
		{
			this.proc = proc;
			// 開いてるIDを見つけるまで(登録できるまで)繰り返す
			for (this.id = 0; RegisterHotKey(this.Handle, this.id, modKeys, keys) == 0; ++this.id)
				if (this.id >= 0xbfff) // 閾値を超えたら失敗
					throw new HotkeyRegistrationException();
		}

		// overrideしHotkeyでの呼び出し時にprocを実行するようにする
		protected override void WndProc(ref Message m)
		{
			base.WndProc(ref m);
			if(m.Msg == WM_HOTKEY)
				if((int)m.WParam == id) proc();
		}

		// ホットキー解除を追加
		protected override void Dispose(bool disposing)
		{
			UnregisterHotKey(this.Handle, id);
			base.Dispose(disposing);
		}
	}

}

// 参考 : http://www.k4.dion.ne.jp/~anis7742/codevault/00140.html
