﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Text;

namespace ProxiedSpaghetti
{
	// Postするやつ
	class Poster
	{
		private Encrypt.AES aes;

		public Poster()
		{
			aes = new Encrypt.AES();

			// ProxyをIEのものにする
			WebRequest.DefaultWebProxy = WebRequest.GetSystemWebProxy();
			// 何かの設定. これがONだと弾かれる.
			ServicePointManager.Expect100Continue = false;
		}

		public class serverUnaccessable : Exception { }

		// 表示内容を返す
		public string post(string text)
		{
			// post設定読み込み
			var svc = Config.loadServerConfig();
			aes.set_key(svc.key, svc.iv);
			
			// accessできるかを確認する
			var ckreq = WebRequest.Create(svc.post_url);
			ckreq.Method = "GET";
			ckreq.Timeout = Config.cconf.timeout;
			try
			{
				var ckres = ckreq.GetResponse();
				var ckbody = new byte[ckres.ContentLength];
				using (var rs = ckres.GetResponseStream()) {
					rs.Read(ckbody, 0, ckbody.Length);
				}
				var mes = Encoding.UTF8.GetString(ckbody);
				Debug.WriteLine("access ckeck result : " + mes);
			}
			catch (WebException we)
			{
				Debug.WriteLine("accessable check failed : " + we.Status);
				throw new serverUnaccessable();
			}

			// post用に暗号化
			var postv = aes.hashAttachedEncrypt(text);

			// generate Post Request
			var req = WebRequest.Create(svc.post_url);
			req.Method = "POST";
			req.ContentLength = postv.Length;
			req.ContentType = "application/octet-stream";
			req.Timeout = Config.cconf.timeout;
			using (var rs = req.GetRequestStream())
			{
				rs.Write(postv, 0, postv.Length);
			}
			
			// post
			var wres = req.GetResponse();
			//Debug.WriteLine("read response " + wres.ToString());
			var res = new byte[wres.ContentLength];
			using (var rs = wres.GetResponseStream()) {
				rs.Read(res, 0, res.Length);
			}
			//Debug.WriteLine("read finish");

			// 復号化内容取得
			return aes.hashAttachedDecryptToString(res);
		}
	}
}
